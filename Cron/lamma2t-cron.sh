#!/bin/bash

# Lamma2T-cron
# Download and optimize weight of forecasts images from Lamma Rete

ImgRootDir="/var/www-vhosts/mm3g.ovh/lamma2t/images"
ImgOptimisedDir="${ImgRootDir}_optimised"

# For wind
# Example http://www.lamma.rete.toscana.it/models/ww3lr/last/wind10.N.14.png
UrlStub_wind="http://www.lamma.rete.toscana.it/models/ww3lr/last"
ImageStub_wind="wind10.N."

# For sea swell
# Example http://www.lamma.rete.toscana.it/models/ww3lr/last/swh.N.16.png
UrlStub_swell="http://www.lamma.rete.toscana.it/models/ww3lr/last"
ImageStub_swell="swh.N."

ImageExt=".png"
ImageExtOptimised=".optimised.png"

function downloadOneImage() {
  # 1st param: URL base
  UrlStub="$1"
  # 2nd param: basename of the file
  ImageStub="$2"
  # 3rd param: index
  i="$3"

  wget -q --passive "${UrlStub}/${ImageStub}${i}${ImageExt}" -O "${ImgRootDir}/${ImageStub}${i}${ImageExt}" &&
    pngcrush -q "${ImgRootDir}/${ImageStub}${i}${ImageExt}" "${ImgOptimisedDir}/${ImageStub}${i}${ImageExtOptimised}"
}

function downloadAllImages() {
  # First seven images are not downloaded because when the model is published,
  # those seven forecasts are in the past (e.g. forecasts from 0h to 7h whereas
  # model is published at 7:30am). Thus :
  # ImgIndexStart: must be 1 + the min value of $myLoopInit in the php script (here 1 + 7 = 8)
  ImgIndexStart=8
  ImgIndexEnd=57

  for i in $(seq ${ImgIndexStart} ${ImgIndexEnd}); do
    downloadOneImage "${UrlStub_wind}" "${ImageStub_wind}" "${i}"
    downloadOneImage "${UrlStub_swell}" "${ImageStub_swell}" "${i}"
  done
}

# 1) Remove previous downloaded images
rm -rf "${ImgRootDir}/*.${ImageExt}"
rm -rf "${ImgOptimisedDir}/*.${ImageExt}"

# 2) Download images
downloadAllImages
