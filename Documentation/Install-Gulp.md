# Install Gulp on Ubuntu 20.04

From https://gulpjs.com/docs/en/getting-started/quick-start

```shell script
sudo apt install nodejs
sudo npm install --global gulp-cli
```

Then from the project:

```shell script
gulp --version
```
